export const EmployeeModalType = {
    EMPLOYEEDETAIL: "employeeDetail",
    EMPLOYEEINFO: "employeeInfo",
    EMPLOYEESKILL: "employeeSkill",
    EMPLOYEESHIFT: "employeeShift"
}