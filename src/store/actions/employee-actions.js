import { NEWEMPLOYEE, EMPLOYEES } from "./action-types";

const addEmployeeDetail = (employeeDetail) => ({
    type: NEWEMPLOYEE.ADD_EMPLOYEE_DETAIL,
    employeeDetail,
});

const addEmployeeInfo = (employeeInfo) => ({
    type: NEWEMPLOYEE.ADD_EMPLOYEE_INFO,
    employeeInfo,
});

const addEmployeeSkill = (employeeSkill) => ({
    type: NEWEMPLOYEE.ADD_EMPLOYEE_SKILL,
    employeeSkill,
});

const addEmployeeShift = (employeeShift) => ({
    type: NEWEMPLOYEE.ADD_EMPLOYEE_SHIFT,
    employeeShift,
});

const resetNewEmployeeForm = () => ({
    type: NEWEMPLOYEE.CLEAR
})

const addEmployee = (employee) => ({
    type: EMPLOYEES.LOAD_SUCCESS,
    employee
})

export { addEmployeeDetail, addEmployeeInfo, addEmployeeSkill, addEmployeeShift, resetNewEmployeeForm, addEmployee }