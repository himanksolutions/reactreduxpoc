import { UI } from "./action-types";

const openModal = (modal) => ({
    type: UI.OPEN_MODAL,
    modal,
});

const closeModal = (modal) => ({
    type: UI.CLOSE_MODAL,
    modal,
});

export { openModal, closeModal }