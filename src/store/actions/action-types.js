export const EMPLOYEES = {
    LOAD_SUCCESS: "LOAD_SUCCESS_EMPLOYEES",
    LOAD_FAILED: "LOAD_FAILED_EMPLOYEES",
    CLEAR: "CLEAR_EMPLOYEES",
}

export const NEWEMPLOYEE = {
    ADD_EMPLOYEE_DETAIL: "ADD_EMPLOYEE_DETAIL",
    ADD_EMPLOYEE_INFO: "ADD_EMPLOYEE_INFO",
    ADD_EMPLOYEE_SKILL: "ADD_EMPLOYEE_SKILL",
    ADD_EMPLOYEE_SHIFT: "ADD_EMPLOYEE_SHIFT",
    CLEAR: "CLEAR_NEW_EMPLOYEE",
}

export const UI = {
    OPEN_MODAL: "OPEN_MODAL",
    CLOSE_MODAL: "CLOSE_MODAL",
};