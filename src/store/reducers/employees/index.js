import { combineReducers } from "redux";
import dataReducer from "./data";

const employeesReducer = combineReducers({
  data: dataReducer,
});

export { employeesReducer };
