import { EMPLOYEES } from "../../actions";

const dataReducer = (state = [], action) => {
  switch (action.type) {
    case EMPLOYEES.CLEAR:
      return [];
    case EMPLOYEES.LOAD_SUCCESS:
      return [...state, action.employee];
    default:
      return state;
  }
};

export default dataReducer;
