import { EmployeeSkillModel } from "../../../models";
import { NEWEMPLOYEE } from "../../actions";
const defaultValue = EmployeeSkillModel;
const employeeSkillReducer = (state = { ...defaultValue }, action) => {
    switch (action.type) {
        case NEWEMPLOYEE.CLEAR:
            return { ...defaultValue };
        case NEWEMPLOYEE.ADD_EMPLOYEE_SKILL:
            return { ...action.employeeSkill };
        default:
            return state;
    }
};

export default employeeSkillReducer;
