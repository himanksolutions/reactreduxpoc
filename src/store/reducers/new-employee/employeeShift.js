import { EmployeeShiftModel } from "../../../models";
import { NEWEMPLOYEE } from "../../actions";

const defaultValue = { shifts: [{ ...EmployeeShiftModel, name: "Supervisor" }] }
const employeeShiftReducer = (state = { ...defaultValue }, action) => {
    switch (action.type) {
        case NEWEMPLOYEE.CLEAR:
            return { ...defaultValue };
        case NEWEMPLOYEE.ADD_EMPLOYEE_SHIFT:
            return { ...action.employeeShift };
        default:
            return state;
    }
};

export default employeeShiftReducer;
