import { EmployeeDetailModel } from "../../../models";
import { NEWEMPLOYEE } from "../../actions";
const defaultValue = EmployeeDetailModel;
const employeeDetailReducer = (state = { ...defaultValue }, action) => {
    switch (action.type) {
        case NEWEMPLOYEE.CLEAR:
            return { ...defaultValue };
        case NEWEMPLOYEE.ADD_EMPLOYEE_DETAIL:
            return { ...action.employeeDetail };
        default:
            return state;
    }
};

export default employeeDetailReducer;
