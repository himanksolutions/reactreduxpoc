import { combineReducers } from "redux";
import employeeDetail from "./employeeDetail";
import employeeInfo from "./employeeInfo";
import employeeSkill from "./employeeSkill";
import employeeShift from "./employeeShift";

const newEmployeeReducer = combineReducers({
    employeeDetail,
    employeeInfo,
    employeeSkill,
    employeeShift
});

export { newEmployeeReducer };
