import { EmployeeInfoModel } from "../../../models";
import { NEWEMPLOYEE } from "../../actions";
const defaultValue = EmployeeInfoModel;
const employeeInfoReducer = (state = { ...defaultValue }, action) => {
    switch (action.type) {
        case NEWEMPLOYEE.CLEAR:
            return { ...defaultValue };
        case NEWEMPLOYEE.ADD_EMPLOYEE_INFO:
            return { ...action.employeeInfo };
        default:
            return state;
    }
};

export default employeeInfoReducer;
