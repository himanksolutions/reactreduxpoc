import { combineReducers } from "redux";
import { employeesReducer } from "./employees";
import { uiReducers } from "./ui";
import { newEmployeeReducer } from "./new-employee";
const rootReducer = combineReducers({
    employees: employeesReducer,
    ui: uiReducers,
    newEmployee: newEmployeeReducer
});

export { rootReducer };
