/* eslint-disable */
import { put, select, takeEvery } from "redux-saga/effects";
import { NEWEMPLOYEE, addEmployee, resetNewEmployeeForm } from "../actions";

const getNewEmployee = (state) => state.newEmployee;

function* handleAddEmployee() {
    const employee = yield select(getNewEmployee);
    yield put(addEmployee(employee));
    yield put(resetNewEmployeeForm());
}

export function* watchAddEmployee() {
    yield takeEvery(NEWEMPLOYEE.ADD_EMPLOYEE_SHIFT, handleAddEmployee);
}
