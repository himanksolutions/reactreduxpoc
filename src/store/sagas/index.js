import { all, fork } from "redux-saga/effects";
import { watchAddEmployee } from './employee-saga';
export default function* rootSaga() {
  yield all([
    fork(watchAddEmployee)
  ]);
}
