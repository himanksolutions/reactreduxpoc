import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper } from '@material-ui/core';
import { Employee } from '../employee';
import { Header, EmployeeData } from '../shared';

const useStyles = makeStyles({
    root: {
        width: '50%',
        height: 400
    }
});

export default function Dashboard() {
    const classes = useStyles();
    return (
        <div className="container">
            <Employee />
            <Header />
            <Paper className={classes.root}>
                <EmployeeData />
            </Paper>
        </div>
    );
}
