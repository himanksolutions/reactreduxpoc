import React from 'react';
import { TextField, MenuItem, Checkbox, Dialog, Grid, DialogActions, DialogContent, DialogTitle, Button, Paper } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { Formik, Form, FieldArray, getIn } from 'formik';
import * as Yup from "yup";
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { closeModal, openModal, addEmployeeShift } from '../../../store';
import { Transition } from '../../shared';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import { EmployeeModalType } from '../../../core';
import { EmployeeShiftModel } from '../../../models';

const validationSchema = Yup.object().shape({
    shifts: Yup.array().of(
        Yup.object().shape({
            name: Yup.string().required("Shift name is required"),
            position: Yup.string().required("Position is required"),
            shiftInTime: Yup.string().required("Shift time is required"),
            shiftOutTime: Yup.string().required("Shift out is required"),
            staggerRDO: Yup.string().required("Shift out is required"),
        })
    )
});

export default function EmployeeShift() {
    const state = useSelector(state => state);
    const modal = state.ui.modal[EmployeeModalType.EMPLOYEESHIFT] || false;
    const formvalues = state.newEmployee.employeeShift;
    const dispatch = useDispatch();
    const handleClose = () => {
        dispatch(closeModal(EmployeeModalType.EMPLOYEESHIFT));
    }

    const handleBack = () => {
        handleClose();
        dispatch(openModal(EmployeeModalType.EMPLOYEESKILL));
    }

    return (
        <Dialog
            className="alert-modal"
            open={modal}
            TransitionComponent={Transition}
            keepMounted
            maxWidth="md"
            onClose={handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle id="alert-dialog-slide-title">New Employee</DialogTitle>
            <Formik
                enableReinitialize
                initialValues={formvalues}
                validationSchema={validationSchema}
                onSubmit={values =>
                    setTimeout(() => {
                        dispatch(addEmployeeShift(values));
                        handleClose();
                    }, 500)
                }
                render={({ values, touched, errors, handleChange }) => (
                    <Form>
                        <FieldArray
                            name="shifts"
                            render={arrayHelpers => (
                                <DialogContent>
                                    <Paper>
                                        {values.shifts.map((shift, index) => {
                                            const name = `shifts[${index}].name`;
                                            const touchedName = getIn(touched, name);
                                            const errorName = getIn(errors, name);

                                            const position = `shifts[${index}].position`;
                                            const touchedPosition = getIn(touched, position);
                                            const errorPosition = getIn(errors, position);

                                            const shiftInTime = `shifts[${index}].shiftInTime`;
                                            const touchedShiftInTime = getIn(touched, shiftInTime);
                                            const errorShiftInTime = getIn(errors, shiftInTime);

                                            const shiftOutTime = `shifts[${index}].shiftOutTime`;
                                            const touchedShiftOutTime = getIn(touched, shiftOutTime);
                                            const errorShiftOutTime = getIn(errors, shiftOutTime);

                                            const mealInTime = `shifts[${index}].mealInTime`;

                                            const mealOutTime = `shifts[${index}].mealOutTime`;

                                            const staggerRDO = `shifts[${index}].staggerRDO`;
                                            const touchedStaggerRDO = getIn(touched, staggerRDO);
                                            const errorStaggerRDO = getIn(errors, staggerRDO);
                                            return (
                                                <div >
                                                    <div className="flex shift-wrap shift-head">
                                                        <TextField
                                                            className="supervisor-in f1"
                                                            variant="outlined"
                                                            name={name}
                                                            value={shift.name}
                                                            error={Boolean(touchedName && errorName)}
                                                            onChange={handleChange}
                                                            disabled
                                                            type="text"
                                                        />
                                                        <div className="pos f1 tc">
                                                            <label for="">Position</label>
                                                            <TextField
                                                                variant="outlined"
                                                                name={position}
                                                                value={shift.position}
                                                                error={Boolean(touchedPosition && errorPosition)}
                                                                onChange={handleChange}
                                                                type="text"
                                                            />
                                                        </div>
                                                        <div className="shift-times f1 tc">
                                                            <label for="">Shift Times</label>
                                                            <div className="flex ac jsb">
                                                                <TextField
                                                                    fullWidth
                                                                    variant="outlined"
                                                                    name={shiftInTime}
                                                                    value={shift.shiftInTime}
                                                                    error={Boolean(touchedShiftInTime && errorShiftInTime)}
                                                                    onChange={handleChange}
                                                                    type="text"
                                                                />
                                                                <TextField
                                                                    fullWidth
                                                                    variant="outlined"
                                                                    name={shiftOutTime}
                                                                    value={shift.shiftOutTime}
                                                                    error={Boolean(touchedShiftOutTime && errorShiftOutTime)}
                                                                    onChange={handleChange}
                                                                    type="text"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="meal-times f1 tc">
                                                            <label for="">Meal Times</label>
                                                            <div className="flex ac jsb">
                                                                <TextField
                                                                    variant="outlined"
                                                                    name={mealInTime}
                                                                    value={shift.mealInTime}
                                                                    onChange={handleChange}
                                                                    type="text"
                                                                />
                                                                <TextField
                                                                    variant="outlined"
                                                                    name={mealOutTime}
                                                                    value={shift.mealOutTime}
                                                                    onChange={handleChange}
                                                                    type="text"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="RDOs f1 tc">
                                                            <label for="">RDOs</label>
                                                            <div className="flex ac jsb">
                                                                <label for="">Su</label>
                                                                <label for="">M</label>
                                                                <label for="">T</label>
                                                                <label for="">W</label>
                                                                <label for="">Th</label>
                                                                <label for="">F</label>
                                                                <label for="">Sa</label>
                                                            </div>
                                                            <div className="flex ac jsb">
                                                                <Checkbox
                                                                    className="RDOs-checkbox"
                                                                    name={`shifts[${index}].sunday`}
                                                                    checked={shift.sunday}
                                                                    onChange={handleChange}
                                                                />
                                                                <Checkbox
                                                                    className="RDOs-checkbox"
                                                                    name={`shifts[${index}].monday`}
                                                                    checked={shift.monday}
                                                                    onChange={handleChange}
                                                                />
                                                                <Checkbox
                                                                    className="RDOs-checkbox"
                                                                    name={`shifts[${index}].tuesday`}
                                                                    checked={shift.tuesday}
                                                                    onChange={handleChange}
                                                                />
                                                                <Checkbox
                                                                    className="RDOs-checkbox"
                                                                    name={`shifts[${index}].wednesday`}
                                                                    checked={shift.wednesday}
                                                                    onChange={handleChange}
                                                                />
                                                                <Checkbox
                                                                    className="RDOs-checkbox"
                                                                    name={`shifts[${index}].thursday`}
                                                                    checked={shift.thursday}
                                                                    onChange={handleChange}
                                                                />
                                                                <Checkbox
                                                                    className="RDOs-checkbox"
                                                                    name={`shifts[${index}].friday`}
                                                                    checked={shift.friday}
                                                                    onChange={handleChange}
                                                                />
                                                                <Checkbox
                                                                    className="RDOs-checkbox"
                                                                    name={`shifts[${index}].saturday`}
                                                                    checked={shift.saturday}
                                                                    onChange={handleChange}
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="stagger f1 tc">
                                                            <label for="">Stagger RDOs</label>
                                                            <TextField
                                                                fullWidth
                                                                variant="outlined"
                                                                select
                                                                name={staggerRDO}
                                                                value={shift.staggerRDO}
                                                                error={Boolean(touchedStaggerRDO && errorStaggerRDO)}
                                                                onChange={handleChange}
                                                                type="text"
                                                            >
                                                                <MenuItem value="N/A">N/A</MenuItem>
                                                            </TextField>
                                                        </div>

                                                        <div className="f1 remove">
                                                            {index > 0 &&
                                                                <RemoveCircleOutlineIcon className="material-icons prime-color" onClick={() => arrayHelpers.remove(index)} />
                                                            }
                                                        </div>
                                                    </div >
                                                    {index === 0 && <div className="hr s-m"></div>}
                                                </div >
                                            )
                                        })};

                                    </Paper >
                                    <Button className="outline add-shift" onClick={() => arrayHelpers.push({ ...EmployeeShiftModel, name: `Shift${values.shifts.length}` })}>
                                        <AddCircleOutlineIcon /> ADD SHIFT
                                    </Button>
                                </DialogContent >
                            )
                            }
                        />
                        <DialogActions>
                            <Grid container>
                                <Grid item xs={6} >
                                    <Button onClick={handleBack} color="primary" className="outline" variant="outlined">
                                        Back
                                    </Button>
                                </Grid>
                                <Grid item xs={6} align="right">
                                    <Button onClick={handleClose} color="primary" className="mr-2 outline link-b">
                                        Cancel
                                    </Button>
                                    <Button variant="contained" type="submit" color="primary">
                                        Submit
                                    </Button>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </Form >
                )}
            />
        </Dialog >
    );
}
