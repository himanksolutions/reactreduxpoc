export { default as EmployeeDetail } from './EmployeeDetail';
export { default as EmployeeInfo } from './EmployeeInfo';
export { default as EmployeeShift } from './EmployeeShift';
export { default as EmployeeSkill } from './EmployeeSkill';