import React from 'react';
import { TextField, Divider, FormGroup, Dialog, DialogActions, DialogContent, DialogTitle, Button, Grid } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { closeModal, openModal, addEmployeeDetail } from '../../../store';
import { Transition } from '../../shared';
import { EmployeeModalType } from '../../../core';

const validationSchema = yup.object({
    employeeName: yup
        .string('Enter employee name')
        .required('Employee name is required'),
    displayName: yup
        .string('Enter display name')
        .required('Display name is required'),
    port: yup
        .string('Enter port')
        .required('port is required'),
    workSite: yup
        .string('Enter work site')
        .required('Work site is required'),
    activeFrom: yup
        .string('Enter Active From')
        .required('Active from is required'),
    description: yup
        .string('Enter description')
        .required('Description is required'),
});

export default function EmployeeDetail() {
    const state = useSelector(state => state);
    const modal = state.ui.modal[EmployeeModalType.EMPLOYEEDETAIL] || false;
    const formvalues = state.newEmployee.employeeDetail;
    const dispatch = useDispatch();
    const handleClose = () => {
        dispatch(closeModal(EmployeeModalType.EMPLOYEEDETAIL));
    }
    const formik = useFormik({
        enableReinitialize: true,
        initialValues: formvalues,
        validationSchema: validationSchema,
        onSubmit: (values) => {
            dispatch(addEmployeeDetail(values));
            handleClose();
            dispatch(openModal(EmployeeModalType.EMPLOYEEINFO));
        },
    });

    return (
        <Dialog
            className="alert-modal"
            open={modal}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-labelledby="employee-detail-title"
            aria-describedby="employee-detail-description"
        >
            <form onSubmit={formik.handleSubmit} autoComplete="off">
                <DialogTitle id="employee-detail-title">New Employee</DialogTitle>
                <DialogContent>
                    <FormGroup aria-label="position" column>
                        <TextField
                            label="Employee Name"
                            name="employeeName"
                            variant="outlined"
                            value={formik.values.employeeName}
                            onChange={formik.handleChange}
                            error={formik.touched.employeeName && Boolean(formik.errors.employeeName)}
                            helperText={formik.touched.employeeName && formik.errors.employeeName}
                            type="text"
                        />
                        <TextField
                            label="Display Name"
                            name="displayName"
                            variant="outlined"
                            value={formik.values.displayName}
                            onChange={formik.handleChange}
                            error={formik.touched.displayName && Boolean(formik.errors.displayName)}
                            helperText={formik.touched.displayName && formik.errors.displayName}
                            type="text"
                        />
                        <Divider />
                        <TextField
                            label="Port"
                            name="port"
                            variant="outlined"
                            value={formik.values.port}
                            onChange={formik.handleChange}
                            error={formik.touched.port && Boolean(formik.errors.port)}
                            helperText={formik.touched.port && formik.errors.port}
                            type="text"
                        />
                        <TextField
                            label="Work Site"
                            name="workSite"
                            variant="outlined"
                            value={formik.values.workSite}
                            onChange={formik.handleChange}
                            error={formik.touched.workSite && Boolean(formik.errors.workSite)}
                            helperText={formik.touched.workSite && formik.errors.workSite}
                            type="text"
                        />
                        <Grid container>
                            <Grid item xs={6} >
                                <TextField
                                    variant="outlined"
                                    name="activeFrom"
                                    label="Active From:"
                                    type="date"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    value={formik.values.activeFrom}
                                    onChange={formik.handleChange}
                                    error={formik.touched.activeFrom && Boolean(formik.errors.activeFrom)}
                                    helperText={formik.touched.activeFrom && formik.errors.activeFrom}
                                />
                            </Grid>
                            <Grid item xs={6} >
                                <TextField
                                    label="Active Through (Opt.)"
                                    name="activeThrough"
                                    variant="outlined"
                                    type="date"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    value={formik.values.activeThrough}
                                    onChange={formik.handleChange}
                                />
                            </Grid>
                        </Grid>
                        <Divider />
                        <TextField
                            label="Description"
                            name="description"
                            variant="outlined"
                            multiline
                            rows={4}
                            type="date"
                            value={formik.values.description}
                            onChange={formik.handleChange}
                            error={formik.touched.description && Boolean(formik.errors.description)}
                            helperText={formik.touched.description && formik.errors.description}
                        />
                    </FormGroup>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary" className="outline link-b">
                        Cancel
                    </Button>
                    <Button color="primary" type="submit" variant="contained">
                        Next
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    );
}
