import React from 'react';
import { TextField, Select, MenuItem, Grid, FormGroup, Dialog, DialogActions, DialogContent, DialogTitle, Button, FormControl, InputLabel } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { closeModal, openModal, addEmployeeInfo } from '../../../store';
import { Transition } from '../../shared';
import { EmployeeModalType } from '../../../core';

const validationSchema = yup.object({
    employeeIDCode: yup
        .string('Enter employee code')
        .required('Employee code is required'),
    workLocation: yup
        .string('Select work location')
        .required('Work location is required'),
});

export default function EmployeeInfo() {
    const state = useSelector(state => state);
    const modal = state.ui.modal[EmployeeModalType.EMPLOYEEINFO] || false;
    const formvalues = state.newEmployee.employeeInfo;
    const dispatch = useDispatch();
    const handleClose = () => {
        dispatch(closeModal(EmployeeModalType.EMPLOYEEINFO));
    }
    const formik = useFormik({
        enableReinitialize: true,
        initialValues: formvalues,
        validationSchema: validationSchema,
        onSubmit: (values) => {
            dispatch(addEmployeeInfo(values));
            handleClose();
            dispatch(openModal(EmployeeModalType.EMPLOYEESKILL));
        },
    });

    const handleBack = () => {
        handleClose();
        dispatch(openModal(EmployeeModalType.EMPLOYEEDETAIL));
    }

    return (
        <Dialog
            className="alert-modal"
            open={modal}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <form onSubmit={formik.handleSubmit}>
                <DialogTitle id="alert-dialog-slide-title">New Employee</DialogTitle>
                <DialogContent>
                    <FormGroup aria-label="position" column>
                        <TextField
                            label="Employee IDCode"
                            name="employeeIDCode"
                            value={formik.values.employeeIDCode}
                            onChange={formik.handleChange}
                            error={formik.touched.employeeIDCode && Boolean(formik.errors.employeeIDCode)}
                            helperText={formik.touched.employeeIDCode && formik.errors.employeeIDCode}
                            variant="outlined"
                            type="text"
                        />
                        <FormControl variant="outlined" className="formcontrls" fullWidth>
                            <InputLabel id="">Work Locations</InputLabel>
                            <Select
                                label="Work Locations"
                                size='small'
                                margin='dense'
                                name="workLocation"
                                value={formik.values.workLocation}
                                onChange={formik.handleChange}
                                error={formik.touched.workLocation && Boolean(formik.errors.workLocation)}
                                helperText={formik.touched.workLocation && formik.errors.workLocation}
                            >
                                <MenuItem value="AAPD">AAPD</MenuItem>
                                <MenuItem value="Anti-Terror/Contraband Enforcement">Anti-Terror/Contraband Enforcement</MenuItem>
                                <MenuItem value="Agriculture">Agriculture</MenuItem>
                                <MenuItem value="Passenger CORE Processing">Passenger CORE Processing</MenuItem>
                                <MenuItem value="FPF">FPF</MenuItem>
                                <MenuItem value="Passenger Admissibility Unit">Passenger Admissibility Unit</MenuItem>
                                <MenuItem value="Passenger Enforcement Rover Team">Passenger Enforcement Rover Team</MenuItem>
                            </Select>
                        </FormControl>
                    </FormGroup>
                </DialogContent>
                <DialogActions>
                    <Grid container>
                        <Grid item xs={6} >
                            <Button onClick={handleBack} color="primary" variant="outlined" className="leftb outline">
                                Back
                            </Button>
                        </Grid>
                        <Grid item xs={6} align="right">
                            <Button onClick={handleClose} color="primary" className="mr-2 outline link-b">
                                Cancel
                            </Button>
                            <Button color="primary" type="submit" className="fill" variant="contained">
                                Next
                            </Button>
                        </Grid>
                    </Grid>
                </DialogActions>
            </form>
        </Dialog>
    );
}
