import React from 'react';
import { TextField, FormGroup, Dialog, DialogActions, DialogContent, DialogTitle, Button, Grid, Chip, IconButton } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { closeModal, openModal, addEmployeeSkill } from '../../../store';
import { Transition } from '../../shared';
import { EmployeeModalType } from '../../../core';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';

const validationSchema = yup.object({
    additionalSkill: yup
        .string('Enter additional skills')
        .required('Additional skills is required'),
});

export default function EmployeeSkill() {
    const state = useSelector(state => state);
    const modal = state.ui.modal[EmployeeModalType.EMPLOYEESKILL] || false;
    const formvalues = state.newEmployee.employeeSkill;
    const dispatch = useDispatch();
    const handleClose = () => {
        dispatch(closeModal(EmployeeModalType.EMPLOYEESKILL));
    }
    const formik = useFormik({
        enableReinitialize: true,
        initialValues: formvalues,
        validationSchema: validationSchema,
        onSubmit: (values) => {
            dispatch(addEmployeeSkill(values));
            handleClose();
            dispatch(openModal(EmployeeModalType.EMPLOYEESHIFT));
        },
    });
    const addSkill = () => {
        const { newSkill, skills } = formik.values;
        formik.setFieldValue("skills", [...skills, newSkill]);
        formik.setFieldValue("newSkill", "");
    }
    const handleDelete = (index) => {
        const { skills } = formik.values;
        skills.splice(index, 1);
        formik.setFieldValue("skills", [...skills]);
    };
    const handleBack = () => {
        handleClose();
        dispatch(openModal(EmployeeModalType.EMPLOYEEINFO));
    }
    return (
        <Dialog
            className="alert-modal"
            open={modal}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <form onSubmit={formik.handleSubmit}>
                <DialogTitle id="alert-dialog-slide-title">New Employee</DialogTitle>
                <DialogContent>
                    <FormGroup aria-label="position" column>
                        <FormGroup aria-label="position" row>
                            {formik.values.skills.map((skill, index) => (
                                <Chip
                                    key={Math.random()}
                                    label={skill}
                                    onDelete={() => handleDelete(index)}
                                    color="secondary"
                                    className="chipcss"
                                />
                            ))}
                        </FormGroup>
                        <FormGroup aria-label="position" row>
                            <Grid item xs={11} >
                                <TextField
                                    fullWidth
                                    label="Employee Skills"
                                    variant="outlined"
                                    name="newSkill"
                                    type="text"
                                    value={formik.values.newSkill}
                                    onChange={formik.handleChange}
                                />
                            </Grid>
                            <Grid item xs={1} >
                                <IconButton onClick={addSkill}>
                                    <AddCircleOutlineIcon />
                                </IconButton>
                            </Grid>
                        </FormGroup>
                        <TextField
                            label="Additional Skill"
                            variant="outlined"
                            name="additionalSkill"
                            multiline
                            rows={4}
                            type="text"
                            value={formik.values.additionalSkill}
                            onChange={formik.handleChange}
                            error={formik.touched.additionalSkill && Boolean(formik.errors.additionalSkill)}
                            helperText={formik.touched.additionalSkill && formik.errors.additionalSkill}
                        />
                    </FormGroup>
                </DialogContent>
                <DialogActions>
                    <Grid container>
                        <Grid item xs={6} >
                            <Button onClick={handleBack} color="primary" variant="outlined" className="outline">
                                Back
                            </Button>
                        </Grid>
                        <Grid item xs={6} align="right">
                            <Button onClick={handleClose} color="primary" className="outline link-b mr-2">
                                Cancel
                            </Button>
                            <Button color="primary" type="submit" className="fill" variant="contained">
                                Next
                            </Button>
                        </Grid>
                    </Grid>
                </DialogActions>
            </form>
        </Dialog>
    );
}
