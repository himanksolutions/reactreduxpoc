import React, { Fragment } from 'react';
import { EmployeeDetail, EmployeeInfo, EmployeeSkill, EmployeeShift } from './forms';


export default function Employee() {
    return (
        <Fragment>
            <EmployeeDetail />
            <EmployeeInfo />
            <EmployeeShift />
            <EmployeeSkill />
        </Fragment>
    );
}