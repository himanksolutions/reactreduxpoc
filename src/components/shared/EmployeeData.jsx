import React from 'react';
import { useSelector } from 'react-redux';
import WarningIcon from '@material-ui/icons/Warning';

export default function EmployeeData() {
    const employees = useSelector(state => state.employees.data);
    return (
        <table className="employee-table">
            <thead>
                <tr>
                    <th>
                        <div className="flex ac">
                            <span> Work Unit </span>
                            <span className="material-icons arr-d">
                            </span>
                        </div>
                    </th>
                    <th>Work Site</th>
                    <th className="tc">Unit Size</th>
                    <th className="tc">Active Until</th>
                    <th className="danger tc">
                        <div className="fc">
                            Flex Capable
                            <WarningIcon className="danger-icon" />
                        </div>
                    </th>
                </tr>
            </thead>
            <tbody>
                {employees.map((employee, index) => (
                    <tr key={index}>
                        <td className="prime-color">{employee.employeeInfo.workLocation}</td>
                        <td>{employee.employeeDetail.workSite}</td>
                        <td className="tc">22/24</td>
                        <td className="tc">{employee.employeeDetail.activeThrough}</td>
                        <td className="tc">No</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}