import React, { Fragment } from 'react';
import { useDispatch } from 'react-redux';
import { ButtonBase, Select, MenuItem, FormGroup, FormControl, InputLabel } from '@material-ui/core';
import { openModal, resetNewEmployeeForm } from '../../store';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import AddIcon from '@material-ui/icons/Add';

export default function Header() {
    const dispatch = useDispatch();
    const handleNewEmployee = () => {
        dispatch(resetNewEmployeeForm());
        dispatch(openModal("employeeDetail"));
    }
    return (
        <Fragment>
            <div className="titleHolder">
                <h1>Employee Manager Portal</h1>
                <ButtonBase className="outline" onClick={handleNewEmployee}>
                    <AddIcon className="icon-plus" /> New Employee
                </ButtonBase>
            </div>
            <div className="filterTop">
                <div className="filterTopTitle">
                    <h3 className="filterTopHeading"><i className="icon-filter"></i>Additional Filters</h3>
                    <ExpandLessIcon />
                </div>
                <FormGroup className="filterTopForm" row>
                    <FormControl variant="outlined" className="formcontrl">
                        <InputLabel id="">Work Site</InputLabel>
                        <Select>
                            <MenuItem value="Active">
                                Active
                            </MenuItem>
                        </Select>
                    </FormControl>
                    <FormControl variant="outlined" className="formcontrl">
                        <InputLabel id="">Status</InputLabel>
                        <Select>
                            <MenuItem value="Active">
                                Active
                            </MenuItem>
                        </Select>
                    </FormControl>
                    <FormControl variant="outlined" className="formcontrl">
                        <InputLabel id="">Flex</InputLabel>
                        <Select>
                            <MenuItem value="Yes">
                                Yes
                            </MenuItem>
                            <MenuItem value="No">No</MenuItem>
                        </Select>
                    </FormControl>
                </FormGroup>
            </div>
        </Fragment>

    );
}