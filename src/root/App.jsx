import React from "react";
import { Provider } from "react-redux";
import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import { MuiThemeProvider, createTheme } from "@material-ui/core/styles";
import { Router, Switch, Route } from "react-router-dom";
import { red } from '@material-ui/core/colors';
import { configureStore, history } from "../store";
import { Dashboard } from "../components/dashboard";

const store = configureStore();
const persistor = persistStore(store);
const theme = createTheme({
  typography: {
    fontSize: 12,
  },
  palette: {
    primary: {
      main: '#80bdff',
    },
    secondary:{
      main: '#007bff',
    },
    error: {
      main: red.A400,
    },
    background: {
      background: '#009387',
    },
    divider : "#000",
    text:{
      primary : "#2E3438"
    }
  },
  root:{
    marginBottom:'0px'
  }
});

const App = () => {
  return (
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Router history={history()}>
            <Switch>
              <Route path="/" component={Dashboard} />
            </Switch>
          </Router>
        </PersistGate>
      </Provider>
    </MuiThemeProvider>
  );
};

export default App;
