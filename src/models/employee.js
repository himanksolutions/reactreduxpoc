
export const EmployeeDetailModel = {
    activeFrom: "",
    activeThrough: "",
    description: "",
    displayName: "",
    employeeName: "",
    port: "",
    workSite: "",
}

export const EmployeeInfoModel = {
    employeeIDCode: "",
    workLocation: ""
}

export const EmployeeSkillModel = {
    additionalSkill: "",
    skills: [],
    newSkill: ""
}

export const EmployeeShiftModel = {
    name: "",
    position: "",
    shiftInTime: "",
    shiftOutTime: "",
    mealInTime: "",
    mealOutTime: "",
    sunday: false,
    monday: false,
    tuesday: false,
    wednesday: false,
    hursday: false,
    friday: false,
    saturday: false,
    staggerRDO: ""
}